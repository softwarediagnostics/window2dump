// Window2Dump version 1.0 (c) Software Diagnostics Technology and Services 

#include "resource.h"

#define _WIN32_WINNT 0x0600

#include <afxext.h>         
#include <string>

using namespace std::string_literals;

#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")

void DisplayMessage(const std::wstring& prefix, DWORD dwStyle, bool fUseGle = true);

wchar_t TITLE[] = L"Window2Dump"; // We need a writable buffer for some API calls.
constexpr wchar_t REG_RUNSECTION[] = L"Options";
constexpr wchar_t REG_COMMAND[] = L"DumperCommand";
constexpr wchar_t REG_COMMAND_DEFAULT[] = L"procdump -ma %d";
constexpr wchar_t REG_TOOLTIPS[] = L"Tooltips";

auto hDeleter = [](auto ph) { if (ph && (*ph != INVALID_HANDLE_VALUE)) ::CloseHandle(*ph); };
using Handle = std::unique_ptr<HANDLE, decltype(hDeleter)>;

class RAII_HANDLE : public Handle
{
public:
	RAII_HANDLE(HANDLE _h) : Handle(&h, hDeleter), h(_h) {};
	void operator= (HANDLE _h) { Handle::reset(); h = _h; Handle::reset(&h); }
	operator HANDLE() const { return h; }
private:
	HANDLE h;
};

class CWindow2DumpApp : public CWinApp
{
public:
	CWindow2DumpApp() {};

private:
	virtual BOOL InitInstance() override;

	DECLARE_MESSAGE_MAP()
};

class CWindow2DumpDlg : public CDialog
{
public:
	CWindow2DumpDlg() : CDialog(CWindow2DumpDlg::IDD, nullptr), m_fTooltips(true)
	{
		m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	};

private:
	enum { IDD = IDD_WINDOW2DUMP_DIALOG };

	virtual void DoDataExchange(CDataExchange* pDX) override
	{
		CDialog::DoDataExchange(pDX);
	};

	virtual BOOL OnInitDialog() override;

	DECLARE_MESSAGE_MAP()

	afx_msg void OnDestroy();
	afx_msg LRESULT OnWmUser(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedCheckTooltips();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnWindowPosChanging(WINDOWPOS * lpwndpos);

	void EnableTooltips();
	void UpdateTooltips();
	void DumpProcess(DWORD pid);

	HICON m_hIcon{};
	UINT_PTR m_Timer{};
	HWND m_hWndTip{};
	POINT m_ptCursorSize{};
	TOOLINFO m_ti{};
	bool m_fTooltips{};
	bool m_visible{false};
	POINT m_ptPrev{};
	
	CString m_strDumperCommand{};
	RAII_HANDLE m_hUserDump{nullptr};
};

CWindow2DumpApp theApp;

BEGIN_MESSAGE_MAP(CWindow2DumpApp, CWinApp)
END_MESSAGE_MAP()

BOOL CWindow2DumpApp::InitInstance()
{
	CWinApp::InitInstance();

	SetRegistryKey(L"Software Diagnistics Technology and Services");

	CWindow2DumpDlg dlg;

	m_pMainWnd = &dlg;
	dlg.DoModal();

	return FALSE;
}

class CAboutDlg : public CDialog
{
public:
	CAboutDlg() : CDialog(CAboutDlg::IDD) {};

private:
	enum { IDD = IDD_ABOUTBOX };

	virtual void DoDataExchange(CDataExchange* pDX) override
	{ 
		CDialog::DoDataExchange(pDX); 
	};

	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

BEGIN_MESSAGE_MAP(CWindow2DumpDlg, CDialog)
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER+1, OnWmUser)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	ON_BN_CLICKED(IDC_CHECK_TOOLTIPS, OnBnClickedCheckTooltips)
	ON_WM_TIMER()
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()

BOOL CWindow2DumpDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	this->ShowWindow(SW_HIDE);

	m_strDumperCommand = ::AfxGetApp()->GetProfileString(REG_RUNSECTION, REG_COMMAND, REG_COMMAND_DEFAULT);
	m_fTooltips = ::AfxGetApp()->GetProfileInt(REG_RUNSECTION, REG_TOOLTIPS, 1);

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	NOTIFYICONDATA nid{};
	nid.cbSize = sizeof(nid);
	nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	nid.hWnd = m_hWnd;
	nid.uID = 1;
	nid.hIcon = m_hIcon;
	nid.uCallbackMessage = WM_USER+1;
	wcscpy_s(nid.szTip, TITLE);

	::Shell_NotifyIcon(NIM_ADD, &nid); 

	PostMessage(WM_COMMAND, IDOK, 0);

	CheckDlgButton(IDC_CHECK_TOOLTIPS, m_fTooltips ? BST_CHECKED : BST_UNCHECKED);
	
	m_hWndTip = ::CreateWindowEx(NULL, TOOLTIPS_CLASS, L"", WS_POPUP, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, nullptr, nullptr, AfxGetInstanceHandle(), nullptr);
	
	if (m_hWndTip)
	{
		::SetWindowPos(m_hWndTip, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

		m_ti.cbSize = sizeof(m_ti);
		m_ti.uFlags = TTF_IDISHWND | TTF_TRACK | TTF_ABSOLUTE;
		m_ti.lpszText = TITLE;
		m_ti.hwnd = m_hWnd;
		m_ti.uId = reinterpret_cast<UINT_PTR>(m_hWnd);
		m_ti.hinst = ::AfxGetInstanceHandle();

		::SendMessage(m_hWndTip, TTM_ADDTOOL, 0, reinterpret_cast<LPARAM>(&m_ti));

		m_ptCursorSize.x = ::GetSystemMetrics(SM_CXCURSOR); 
		m_ptCursorSize.y = ::GetSystemMetrics(SM_CYCURSOR); 

		if (m_fTooltips)
		{
			EnableTooltips();
		}
	}

	SetDlgItemText(IDC_EDIT_DUMPER, m_strDumperCommand.GetBuffer());

	m_Timer = ::SetTimer(m_hWnd, 1, 500, nullptr); 
	
	return TRUE;
}

void CWindow2DumpDlg::OnDestroy()
{
	CDialog::OnDestroy();

	KillTimer(m_Timer);

	if (m_hWndTip && m_fTooltips)
	{
		::SendMessage(m_hWndTip, TTM_TRACKACTIVATE, 0 , reinterpret_cast<LPARAM>(&m_ti));
	}

	NOTIFYICONDATA nid{0};
	nid.cbSize = sizeof(nid);
	nid.hWnd = m_hWnd;
	nid.uID = 1;

	::Shell_NotifyIcon(NIM_DELETE, &nid); 

	::AfxGetApp()->WriteProfileString(REG_RUNSECTION, REG_COMMAND, m_strDumperCommand);
	::AfxGetApp()->WriteProfileInt(REG_RUNSECTION, REG_TOOLTIPS, m_fTooltips);
}

LRESULT CWindow2DumpDlg::OnWmUser(WPARAM wParam, LPARAM lParam)
{
	if (lParam == WM_RBUTTONUP)
	{
		POINT pt;
		::GetCursorPos(&pt);
		
		HMENU hMenu = ::LoadMenu(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MENU));
		
		switch (::TrackPopupMenu(::GetSubMenu(hMenu, 0), TPM_RETURNCMD | TPM_NONOTIFY, pt.x, pt.y, 0, m_hWnd, nullptr))
		{
		case ID_OPTIONS:
			m_visible = true;
			ShowWindow(SW_SHOW);
			break;	

		case ID_EXIT:
			CDialog::OnOK();
			break;
		
		case ID_ABOUT:
			CAboutDlg dlgAbout;
			dlgAbout.DoModal();
			break;
		}
		
		::DestroyMenu(hMenu);
	}

	return 0;
}
void CWindow2DumpDlg::OnBnClickedOk()
{
	GetDlgItemText(IDC_EDIT_DUMPER, m_strDumperCommand);
	ShowWindow(SW_HIDE);
}

void CWindow2DumpDlg::OnBnClickedCancel()
{
	ShowWindow(SW_HIDE);
}

void CWindow2DumpDlg::OnBnClickedCheckTooltips()
{
	m_fTooltips = !m_fTooltips;
	
	if (m_hWndTip)
	{
		if (!m_fTooltips)
		{
			::SendMessage(m_hWndTip, TTM_TRACKACTIVATE, 0, reinterpret_cast<LPARAM>(&m_ti));
		}
		else
		{
			EnableTooltips();	
		}
	}
}

void CWindow2DumpDlg::OnWindowPosChanging(WINDOWPOS * lpwndpos)
{
	if (!m_visible)
	{
		lpwndpos->flags &= ~SWP_SHOWWINDOW;
	}

	CDialog::OnWindowPosChanging(lpwndpos);
}
void CWindow2DumpDlg::OnTimer(UINT_PTR nIDEvent)
{
	CDialog::OnTimer(nIDEvent);

	if (m_hWndTip)
	{
		UpdateTooltips();
	}

	if (::GetAsyncKeyState(VK_LSHIFT) && ::GetAsyncKeyState(VK_RSHIFT))
	{
		POINT pt{};
		::GetCursorPos(&pt);
		
		if (HWND child = ::WindowFromPoint(pt); IsWindow(child))
		{
			DWORD pid{};
			::GetWindowThreadProcessId(child, &pid);
			DumpProcess(pid);
		}
	}
}

void CWindow2DumpDlg::UpdateTooltips()
{
	POINT pt{};
	::GetCursorPos(&pt);

	if (pt.x != m_ptPrev.x && pt.y != m_ptPrev.y)
	{
		if (HWND hWnd = ::WindowFromPoint(pt); ::IsWindow(hWnd))
		{
			CString tooltip{L"[Window2Dump - <LeftShift-RightShift>]\n"};
			CString line;
			
			WCHAR szName[MAX_PATH]{0};
			::RealGetWindowClass(hWnd, szName, MAX_PATH);
			line.Format(L"HWND: %x Class: %s \n", hWnd, szName);
			tooltip += line.GetBuffer();

			HWND hWndParent = hWnd;
			while (::GetParent(hWndParent))
			{
				hWndParent = ::GetParent(hWndParent);
				::RealGetWindowClass(hWndParent, szName, MAX_PATH);
				line.Format(L"Parent HWND: %x Class: %s \n", hWndParent, szName);
				tooltip += line.GetBuffer();
			}

			DWORD dwProcessID{};
			DWORD dwThreadID = ::GetWindowThreadProcessId(hWnd, &dwProcessID);
			line.Format(L"PID.TID: %x.%x (%d.%d) \n", dwProcessID, dwThreadID, dwProcessID, dwThreadID); 
			tooltip += line.GetBuffer();

			RAII_HANDLE hProcess = ::OpenProcess(PROCESS_QUERY_INFORMATION, 0, dwProcessID);
			DWORD dwSize{MAX_PATH};
			::QueryFullProcessImageName(hProcess, 0, szName, &dwSize);
			line.Format(L"Path: %s ", szName); 
			tooltip += line.GetBuffer();
			
			m_ti.lpszText = tooltip.GetBuffer();
			
			::SendMessage(m_hWndTip, TTM_SETMAXTIPWIDTH, 0, 320);
			::SendMessage(m_hWndTip, TTM_UPDATETIPTEXT, 0, reinterpret_cast<LPARAM>(&m_ti));
			
			POINT ptNew = {pt.x+m_ptCursorSize.x/2, pt.y+m_ptCursorSize.y/2};
			RECT rcTip{};
			::GetWindowRect(m_hWndTip, &rcTip);
			
			if (ptNew.x + (rcTip.right - rcTip.left) > ::GetSystemMetrics(SM_CXSCREEN))
			{
				ptNew.x = pt.x - (rcTip.right - rcTip.left) - m_ptCursorSize.x/4;
			}
			
			if (rcTip.bottom + (rcTip.bottom - rcTip.top) > ::GetSystemMetrics(SM_CYSCREEN))
			{
				ptNew.y = pt.y - (rcTip.bottom - rcTip.top) - m_ptCursorSize.y/4;
			}
			
			::SendMessage(m_hWndTip, TTM_TRACKPOSITION, 0, MAKELPARAM(ptNew.x, ptNew.y));
		}

		m_ptPrev = pt;
	}
}

void CWindow2DumpDlg::EnableTooltips()
{
	if (m_hWndTip)
	{
		::SendMessage(m_hWndTip, TTM_TRACKACTIVATE, TRUE, reinterpret_cast<LPARAM>(&m_ti));
		UpdateTooltips();
	}
}

void CWindow2DumpDlg::DumpProcess(DWORD pid)
{
	if (!m_strDumperCommand.GetLength())
	{
		return;
	}

	if (m_hUserDump)
	{
		if (::WaitForSingleObject(m_hUserDump, 1000) == WAIT_TIMEOUT)
		{
			DisplayMessage(L"Process dumper is still saving the process", MB_ICONWARNING);
			return;
		}
		
		m_hUserDump.reset();
	}

	int cch{0};
	bool illegal{false};
	WCHAR *pch = m_strDumperCommand.GetBuffer(); 
	for (int i{0}; i < m_strDumperCommand.GetLength() - 1; ++i)
	{
		if (pch[i] == L'%')
		{
			++cch;
			if (pch[i+1] != L'd')
			{
				illegal = true;
				break;
			}
		}
	}

	if (cch > 1 || illegal)
	{
		DisplayMessage(L"Process dumper command line has invalid format: \n"s + m_strDumperCommand.GetBuffer(), MB_ICONERROR, false);
		return;
	}

 	STARTUPINFO si{0};
	PROCESS_INFORMATION pi{0};
	CString cmd;

	if (m_strDumperCommand.Find(L"%d") == -1)
	{
		cmd = m_strDumperCommand;
	} 
	else
	{
		cmd.Format(m_strDumperCommand, pid);
	}

	if (::CreateProcess(nullptr, cmd.GetBuffer(), nullptr, nullptr, 0, 0, nullptr, nullptr, &si, &pi))
	{
		::CloseHandle(pi.hThread);
		m_hUserDump = pi.hProcess;
	}
	else
	{
		DisplayMessage(L"CreateProcess failed: "s + cmd.GetBuffer(), MB_ICONERROR);
	}
}

void DisplayMessage(const std::wstring& prefix, DWORD dwStyle, bool fUseGle)
{
	std::wstring message(prefix);

	if (DWORD gle = ::GetLastError(); fUseGle && gle)
	{
		if (LPWSTR pErrMsg; ::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, nullptr, gle, 0,
			reinterpret_cast<decltype(pErrMsg)>(&pErrMsg), 0, nullptr) && pErrMsg)
		{
			message += L"\n"s + pErrMsg;
			::LocalFree(pErrMsg);
		}
	}

	::MessageBox(nullptr, message.c_str(), TITLE, dwStyle | MB_OK | MB_SETFOREGROUND | MB_TOPMOST);
}
